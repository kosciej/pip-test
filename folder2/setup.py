import setuptools


setuptools.setup(
    name="funkcja2",
    version="0.0.1",
    author="arocketman",
    author_email="and.capuano@gmail.com",
    description="It's pip... with git.",
    # long_description=long_description,
    url="https://gitlab.com/kosciej/pip-test.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
