import setuptools


setuptools.setup(
    name="dupa",
    version="0.0.1",
    author="arocketman",
    author_email="and.capuano@gmail.com",
    description="It's pip... with git.",
    # long_description=long_description,
    packages=setuptools.find_packages(include=["folder1", "folder1.*"]),
    install_requires=[
        "funny",
    ],
    extras_require={
        "interactive": ["psutil"],
    },
)
